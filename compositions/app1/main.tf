resource "null_resource" "re-image" {
  triggers = {
    always = timestamp()
  }
  provisioner "local-exec" {
    command = "ls"
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_ssm_parameter" "test" {
  #checkov:skip=CKV2_AWS_34:  disabled alarm
  name  = "/test/password"
  type  = "String"
  value = "password123"
}


